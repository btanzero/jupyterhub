# Start from a core stack version
FROM jupyter/minimal-notebook:python-3.8.8
USER root
RUN apt-get update && apt-get install -y gcc unixodbc-dev && apt-get install -y libboost-locale-dev
# Install other apt packages
RUN apt-get update && apt-get install -y graphviz
USER $NB_UID

# Install from requirements.txt file
COPY --chown=${NB_UID}:${NB_GID} full-requirements.txt /tmp/
RUN pip install --requirement /tmp/full-requirements.txt --no-cache-dir && \
    fix-permissions $CONDA_DIR && \
    fix-permissions /home/$NB_USER

# Point to outpost.ws pypi mirror
COPY ./pip.conf /etc/
COPY --chown=${NB_UID}:${NB_GID} ./DenodoDialectSQLAlchemy.zip /tmp
RUN unzip /tmp/DenodoDialectSQLAlchemy.zip -d /opt/conda/lib/python3.8/site-packages/sqlalchemy/ && \
    mv /opt/conda/lib/python3.8/site-packages/sqlalchemy/denodo-sqlalchemy-dialect-8.0-20201204/denodo /opt/conda/lib/python3.8/site-packages/sqlalchemy/dialects/ && \
    rm -rf /opt/conda/lib/python3.8/site-packages/sqlalchemy/denodo-sqlalchemy-dialect-8.0-20201204 && \
    chattr -i -a /tmp/DenodoDialectSQLAlchemy.zip && \
    chmod ugo+w /tmp/DenodoDialectSQLAlchemy.zip && \
    rm /tmp/DenodoDialectSQLAlchemy.zip

COPY --chown=${NB_UID}:${NB_GID} ./denodo-vdp-jdbcdriver-8.0-update-20210209.jar /tmp
RUN mkdir -p /opt/denodo/8.0/tools/client-drivers/jdbc && \
    mv /tmp/denodo-vdp-jdbcdriver-8.0-update-20210209.jar /opt/denodo/8.0/tools/client-drivers/jdbc/denodo-vdp-jdbcdriver-8.0-update-20210209.jar
